import os.path
import praw
import sqlite3
from time import localtime, strftime

r = praw.Reddit('Python 3.5 - TIL PRAW!')
subreddit = r.get_subreddit('todayilearned')

for i, submission in enumerate(subreddit.get_hot(limit=25)):
    if not submission.stickied:
        print('#{}'.format(i))
        print('id:', submission.id)
        # print(submission.permalink)
        print(submission.url)
        print(submission.title)
        print('score:', submission.score)
        print(strftime('%Y-%m-%d %H:%M:%S', localtime(submission.created_utc)))
        print(strftime('%Y-%m-%d %I:%M:%S %p', localtime(submission.created_utc)))
        print('downvotes:', submission.downs)
        print('upvotes:  ', submission.ups)
        print()
    else:
        print(dir(submission))
